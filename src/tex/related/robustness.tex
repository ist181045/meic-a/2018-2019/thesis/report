\subsection{Robustness}
\label{sec:related.robustness}

The correctness proofs of nearly all geometric algorithms presented in
theoretical papers assumes exact computation with real numbers
\cite{cgal:bfghhkps-lgk23-18b}. However, floating-point numbers are represented
with fixed precision in computers, making them inexact, which leads to
inaccurate representations of the conceptual real number counterparts. For
example, the rational number one-tenth ($\frac{1}{10}$) cannot be accurately
represented as a floating-point number, nor is it guaranteed to be truly equal
to another seemingly identical number. Such comparisons must be performed
relying on tolerances, i.e., if $a$ and $b$ are two floating-point numbers, they
are considered \textit{the same} if $|a - b| \le \epsilon$ for a given tolerance
$\epsilon$.

As an example, consider the problem of, given two points $P,Q \in \mathbb{R}^2$,
finding the closest point to the origin. The distance between two points can be
expressed by
%
\begin{equation}
  d(P, Q) = \sqrt{(x_Q - x_P)^2 + (y_Q - y_P)^2}.
\end{equation}
%
To determine the closest point, we compare both points' distance to the origin
$O$. That is, if
%
\[
  d(P, O) < d(Q, O)
\]
%
holds, $P$ is the closest to the origin. Otherwise, they are either equidistant
or $Q$ is closer. However, applying the square root operation is a step that
will further introduce errors in the computation process. Given that we are only
interested in comparing distances, and not use their actual value, we can,
instead, compare the squared distances. As such, we avoid the square root, thus
improving robustness, despite the limitations with fixed precision arithmetic,
and speeding up the process becuase the square root is a computationally heavy
operation. \citet{Mei:2014:NRGC} further discuss the issues with numerical
robustness in geometric computation, namely how they arise, and propose
practical solutions.

When used without care, fixed-precision arithmetic almost always leads to
unwanted results due to marginal error accumulation caused by rounding
(\textit{roundoff}), propagated throughout a series of calculations. As seen
above, careful observations must be made before proceeding with computations as
simple as distance calculation. To help solve this problem, more robust
numerical constructs and concepts can be used. In particular, exact numbers,
such as rational numbers or arbitrary precision numbers, the latter also known
as \textit{bignums}, allow arbitrary-precision arithmetic, capable of
representing numbers with virtually infinite precision with the drawback that
arithmetic operations are slower, however mitigating precision issues, providing
more accurate constructs and improving code robustness.

Several libraries already strive to implement robust geometric computation. One
such example is the \acf{CGAL} \cite{CGAL:2018}. \ac{CGAL} is a comprehensive
library that employs an exact computation paradigm \cite{Yap:1995:ECP},
producing correct results despite roundoff errors and properly handling
\textit{degenerate} situations (e.g., 3D points on a 2D plane), relying on
numbers with arbitrary precision to do so. Moreover, other libraries, such as
\acs{LEDA}\label{acro:LEDA} \cite{LEDA:2017,Mehlhorn:1989:LEDA}, and CORE
\cite{Karamcheti:1999:CLRNGC} and its successor \cite{Yu:2010:CORE2}, also deal
with robustness problems in geometric computation, offering simpler interfaces
when compared to \ac{CGAL}. However, \ac{CGAL} arguably remains the \textit{de
facto} library for robust exact geometric computation.
