\subsection{Constraints in \acs{CAD}}
\label{sec:intro.constraints}

We've seen how parametric operations in \ac{CAD} software have evolved. These
operations allow the user to create geometric objects that satisfy certain
constraints \textit{implicitly} imposed on the objects when the user selects the
operations they want to use along with the respective operation's inputs.
Naturally, \ac{GCS} fits well in \ac{CAD} applications. Geometric constraints
allow the repositioning and scaling of geometric objects so that they satisfy
constraints \textit{explicitly} imposed on them by the user.

\acp{CSP} are a well-known subject of research both in mathematics and in the
\ac{CS} field. \ac{GCS} is a subclass of a \ac{CSP}, i.e., it is a \ac{CSP} in a
computational geometry setting. Over the past few years, several approaches to
\ac{GCS} have been researched and developed, significantly expanding the scope
of constraint solvers. The abstract problem of \ac{GCS} is often described as
follows \cite{Bettig:2011:GCSPC:1.3593408}:
%
\begin{quote}
  Given a set of geometric objects, such as points, lines, and circles; a set of
  geometric and dimensional constraints, such as distance, tangency, and
  perpendicularity; and an ambient space, usually the Euclidean plane; assign
  coordinates to the geometric objects such that the constraints are satisfied,
  or report that no such assignment has been found.
\end{quote}
%
The solver's \textit{competence} is related to the capability of reporting
unsolvability: if in fact no solution for the problem at hand exists and the
solver is capable of reporting unsolvability in that case, the solver is deemed
fully competent. Since constraint solving is mostly an exponentially complex
problem \cite{Rossi:2006:Handbook}, partial competence suffices as long as
decent solutions can be found in affordable time and space.

The main approaches to constraint solving are graph-based, logic-based,
algebraic, and theorem prover-based, of which the first is the predominant one.
Some of the subjects approached here are briefed in \cite{Hoffmann:2005:BCS}. In
the following subsections, these different approaches are presented.

\subsubsection{Graph-Based Approaches}
\label{sec:intro.constraints.graph}
The problem is translated into a labeled \textit{constraint graph}, where
vertices are constrained geometric objects, and edges the constraints
themselves. This approach is split into three main branches:
%
\begin{itemize}
  \item[] \textbf{Constructive Approaches} The graph is decomposed and
  recombined to extract basic construction steps that must be solved, where a
  subsequent phase elaborates on this, employing algebraic and/or numerical
  methods. This has become the dominant approach to geometric constraint
  solving, also becoming the target of considerable research and development
  \cite{Bettig:2011:GCSPC:1.3593408}.
  
  \item[] \textbf{Degrees of Freedom Analysis} The graph's vertices are labeled
  with represented object's degrees of freedom. Each edge is labeled by the
  degrees of freedom the constraint cancels out. This graph is then analyzed for
  a solution strategy.
  
  A symbolic solution method is derived using rules with geometric meaning, a
  method proved to be correct in \cite{Kramer:1990:SGCS}. It is further extended
  by using it along with numerical methods as a fallback if geometric reasoning
  fails \cite{Hsu:1997:HCSEIGC}.
  
  \citet{Latham:1996:CA} decompose the graph into minimal connected components
  they call \textit{balanced sets} that are solved by a geometric construction,
  falling back to a numerical solution attempt. This method can deal with
  symbolic constraints and identifies under- and overconstrained problems, where
  the latter kind is approached by prioritizing the given constraints.
  
  \item[] \textbf{Propagation Approaches} The graph's vertices represent
  variables and equations, and the edges are labeled with occurrences of the
  variables in equations. The goal is to orient the graph such that all incident
  edges to an equation vertex but one are incoming edges. If so, the equation
  system has been triangularized. Orientation algorithms include
  degree-of-freedom propagation and propagation of known values
  \cite{Freeman:1990:ICS,Veltkamp:1992:Geometric} which can fail in the presence
  of orientation loops, but such situations are addressed
  \cite{Veltkamp:1992:Geometric} and they may resort to numerical solvers.
\end{itemize}

\subsubsection{Logic-Based Approaches}
\label{sec:intro.constraints.logic}

The constraint problem is translated into a set of geometric assertions and
axioms which is then transformed in such a way that specific solution steps are
made explicit by applying geometric reasoning. The solver then takes a set of
construction steps and assigns coordinate values to the geometric entities.

A geometric loci\footnote{Plural form of locus. In mathematics, a locus is a set
of points that satisfy some condition. In layman's terms, a location or place.}
at which constrained elements must be is obtained using first order logic to
derive geometric information, applying a set of axioms from Hilbert's geometry
\cite{Aldefeld:1988:VGBGRM,Bruderlin:1993:USGRRSGSS,Sohrt:1991:IC3DM}. Two
different types of constraints are further considered
\cite{Sunde:1987:CADSDSS,Verroust:1992:RMPCAD}: (1) sets of points placed with
respect to a local coordinate frame, and (2) sets of straight line segments
whose directions are fixed. The reasoning is performed by applying a rewriting
system on the sets of constraints. Once every geometric element is in a unique
set, the problem is solved.

\subsubsection{Algebraic Approaches}
\label{sec:intro.constraints.algebraic}

The problem is translated into a system of equations where the variables are
coordinates of geometric elements and the equations, which are generally
nonlinear, express the constraints upon them. This approach's main advantage is
its completeness and dimension independence. However, it is difficult to
decompose the equation system into subproblems, and a general, complete solution
of algebraic equations is inefficient. Nonetheless, small algebraic systems
tend to appear in the other approaches and are routinely solved.

\subsubsection{Symbolic Methods}
\label{sec:intro.constraints.symbolic}

General equation solvers employ symbolic techniques to triangularize the
equation system \cite{Buchberger:1985:Grobner,Chou:1988:IWMMTPG}. A solver built
on top of the Buchberger's algorithm is described in \cite{Buchanan:1993:CDS};
\citet{Kondo:1992:AMMDRGM} further reports a symbolic algebraic method.

These methods are powerful since they can produce generic solutions if
constraints are used symbolically, which can be evaluated for a different set of
constraint assignments, then producing parameterized solutions. However, solvers
are very slow and computations demand a lot of space, usually requiring
exponential running time \cite{Durand:1998:SNTCS}.

\subsubsection{Numerical Methods}
\label{sec:intro.constraints.numerical}

Among the oldest approaches to constraint solving, these solve large systems of
equations iteratively. Methods like Newton iteration work properly if a good
approximation of the intended solution can be supplied and the system is not
ill-conditioned. If the starting point comes from the user's sketch, then it
should be close to what is intended. Alas, such methods may find only one
solution, and may not allow the user to select the intended one, whereas the
underlying problem may have more than one.

Alternatively, a relaxation method can be employed
\cite{Borning:1989:PLATL,Hillyard:1978:CNSTDT,Sutherland:1964:Sketchpad}.
However, in general, convergence to a solution is slow.

The Newton-Raphson iteration method, the most widely used one, is a local method
and converges much faster than relaxation, but does not apply to
over-constrained systems of equations unless expanded upon
\cite{Dedieu:2000:Newton}.

Global and guaranteed convergence can be had resorting to the \textit{Homotopy
continuation} family of methods \cite{Allgower:1993:CPF}. Despite usage in
\ac{GCS} \cite{Durand:1998:SNTCS,Lamure:1996:SGCH}, these are far less efficient
than the Newton-Raphson method due to the latter's exhaustive nature.

\subsubsection{Theorem Proving}
\label{sec:intro.constraints.proving}

\ac{GCS} can be seen as a subproblem of geometric theorem proving, but the
latter requires general techniques, therefore requiring much more complex
methods than those required by the former.

Wu's method is an algebraic-based method that can be used to automatically find
necessary conditions to obtain non-degenerated solutions. It can be used to
prove novel geometric theorems \cite{Chou:1988:IWMMTPG}.
\citet{Chou:1996:AGRPGI:1,Chou:1996:AGRPGI:2} develop on automatic geometric
theorem proving, allowing the interpretation of the computed proof.

\subsubsection{Other Areas}
\label{sec:intro.constraints.other}

The following are briefly described key advances made during the past two
decades that interface with other areas or that cannot be readily integrated
into graph-constructive solvers. These techniques also constitute examples of
further attempts to broaden the scope of \ac{GCS}, proving that it is a strong
field of research with many applications beyond \ac{CAD}.

\begin{itemize}
  \item[] \textbf{Deformations} When restrictions are placed on the type of
          deformation, these problems can be seen as constraint solving. For
          example, \cite{Ahn:2011:LEQBCA,Bao:2010:BIVCMSE,Moll:2006:PPDLO}
          consider deformations that minimize strain energy;
          \cite{Xu:2009:SDUAC} entails surface deformation under area
          constraints. However, such techniques are rarely integrated with other
          geometric constraints such as point distance or perpendicularity.
  \item[] \textbf{Dynamic Geometry} The addition of constraints in a given
          underconstrained system can make it well-constrained, and such
          constraints can be seen as parameters when they are dimensional.
          Varying their values, different solutions arise, which can be wholely
          understood as a dynamic geometric configuration. Systems akin to
          Cinderella \cite{Richter:2012:Cinderella.2} can deal with these
          problems. Further literature exists on these problems from a
          constraint solving perspective \cite{Freixas:2010:CDGS}.
  \item[] \textbf{Evolutionary Methods} Consist of re-interpreting the problem
          as an optimization problem, attacking it using genetic, particle-swarm
          or other evolutionary methods
          \cite{Chunhong:2006:PDBOEA,Li:2012:HASPSOASGCP}.
\end{itemize}